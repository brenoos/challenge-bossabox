require('dotenv').config();

/**
 * Retorna o valor de determinada variável ambiente ou um valor padrão qualquer.
 *
 * @param {String} key Identificador da variável
 * @param {*} defaultValue Valor padrão caso não tenha sido definida
 *
 * @return {*}
 */
const getEnv = (key, defaultValue = null) => {
  if (typeof process.env[key] === 'undefined') {
    return defaultValue;
  }
  return process.env[key];
};

/**
 * Retorna o valor de determinada variável ou falha.
 *
 * @param {String} key Identificador da variável
 *
 * @return {*}
 */
const getEnvOrFail = key => {
  const value = getEnv(key);
  if (!value) {
    throw new Error(`Required env variable not set: ${key}`);
  }

  return value;
};

module.exports = {
  database: getEnvOrFail('DATABASE'),
  port: getEnv('PORT', '3000')
};
