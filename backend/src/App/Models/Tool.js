const mongoose = require('mongoose');
require('mongoose-type-url');

const ToolSchema = new mongoose.Schema(
  {
    title: { type: String, required: true },
    link: mongoose.SchemaTypes.Url,
    description: String,
    tags: [{ type: String, required: true }]
  },
  { timestamps: true }
);

module.exports = mongoose.model('Tool', ToolSchema);
