const ToolModel = require('../Models/Tool');

class ToolRepository {
  constructor() {
    this.Tool = ToolModel;
  }

  /**
   * Se não tiver nenhuma query string, retorna todas as tools
   * @param {Object} query
   *
   */
  search(query) {
    return this.Tool.find(query);
  }

  /**
   * Cria uma nova Tool
   * @param {Object} payload
   */
  create(payload) {
    return this.Tool.create(payload);
  }

  /**
   * Procura por uma tool
   * @param {String} id
   */
  find(id) {
    return this.Tool.findById(id);
  }

  /**
   * Remove uma tool
   * @param {String} id
   */
  delete(id) {
    return this.Tool.findByIdAndDelete(id);
  }
}

module.exports = new ToolRepository();
