const toolRepository = require('../../Repositories/ToolRepository');

class ToolController {
  /**
   * Lista todas as tools
   *
   * GET /tool
   * @param {Object} req
   * @param {Request} req.query
   * @param {Object} res
   */
  async index({ query }, res) {
    const tools = await toolRepository.search(query);

    return res.json(tools);
  }

  /**
   * Cria uma tool
   *
   * POST /tool
   * @param {Object} req
   * @param {Request} req.body
   * @param {Object} res
   */
  async create({ body }, res) {
    const tool = await toolRepository.create(body);

    return res.json(tool);
  }

  /**
   * Exibe uma tool específica
   *
   * GET /tool/:id
   * @param {Object} req
   * @param {Request} req.body
   * @param {Object} res
   */
  async show(req, res) {
    const { id } = req.params;

    const fetchedTool = await toolRepository.find(id);

    return res.json(fetchedTool);
  }

  async destroy(req, res) {
    const { id } = req.params;

    const removedTool = await toolRepository.delete(id);

    return res.json(removedTool);
  }
}

module.exports = new ToolController();
