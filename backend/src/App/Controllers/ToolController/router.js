const router = require('express').Router();
const ToolController = require('./index');

module.exports = router
  .post('/', ToolController.create)
  .get('/', ToolController.index)
  .get('/:id', ToolController.show)
  .delete('/:id', ToolController.destroy);
