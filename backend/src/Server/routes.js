const routes = require('express').Router();
const ToolsRoutes = require('../App/Controllers/ToolController/router');

routes.get('/', (req, res) => res.json({ hello: 'World' }));

routes.use('/tools', ToolsRoutes);

module.exports = routes;
