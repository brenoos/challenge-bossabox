const mongoose = require('mongoose');
const app = require('./app');
const config = require('../Config');

mongoose.connect(config.get('database'), { useNewUrlParser: true });

const port = config.get('port');

app.listen(port, () => {
  console.log(`Server running on port ${port}`); //eslint-disable-line
});
