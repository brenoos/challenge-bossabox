const isPlainObject = require('lodash/isPlainObject');
const get = require('lodash/get');
const configObject = require('../../config');

class Config {
  constructor(config) {
    if (isPlainObject(config) === false) {
      throw new Error('Expected config to be a plain object');
    }
    this.config = config;
  }

  get(path, defaultValue = null) {
    return get(this.config, path, defaultValue);
  }
}

module.exports = new Config(configObject);
